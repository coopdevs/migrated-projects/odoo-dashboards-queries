{
    'name': "Odoo dashboard module for queries",
    'version': '12.0.0.0.0-rc2',
    'depends': ['kpi_dashboard','kpi_dashboard_altair'],
    'author': "Coopdevs Treball SCCL",
    'website': 'https://coopdevs.org',
    'category': "Business Intelligence",
    'description': """
    Odoo dashboard module for queries for dashboards
    """,
    "license": "AGPL-3",
    'data': [
        'data/kpi_kpi.xml',
    ],
    'installable': True,
    'application': False,
    'auto_install': False,
}
