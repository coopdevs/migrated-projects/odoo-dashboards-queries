from odoo import models, fields, api
import pandas
import altair
import json

class KpiDashboard(models.Model):
    _inherit = 'kpi.dashboard'

    def super_vendes_totals(self):
        query ="""
         select sum(amount_total) as value
         from sale_order so;
        """
        self.env.cr.execute(query)
        res = self.env.cr.fetchall()
        result = {"value": res[0][0]}
        return result


    def super_vendes_ytd(self):
        query ="""
         select sum(amount_total) as value
         from sale_order so
         where date_order>=date_trunc('year', current_date-interval '1 day'  ) and date_order<current_date;
        """
        self.env.cr.execute(query)
        res = self.env.cr.fetchall()

        query ="""
         select sum(amount_total) as value
         from sale_order so
         where date_order>=date_trunc('year', current_date-interval '1 day'- interval '1 year' ) and date_order<current_date- interval '1 year';
        """
        self.env.cr.execute(query)
        resp = self.env.cr.fetchall()

        result = {"value": res[0][0], "previous": resp[0][0]}
        return result

    def super_vendes_wtd(self):
        query ="""
         select sum(amount_total) as value
         from sale_order so
         where date_order>=date_trunc('week', current_date-interval '1 day'  ) and date_order<current_date;
        """
        self.env.cr.execute(query)
        res = self.env.cr.fetchall()

        query ="""
         select sum(amount_total) as value
         from sale_order so
         where date_order>=date_trunc('year', current_date-interval '1 day' - interval '1 week' ) and date_order<current_date-interval '1 day'- interval '1 week';
        """
        self.env.cr.execute(query)
        resp = self.env.cr.fetchall()

        result = {"value": res[0][0], "previous": resp[0][0]}
        return result

    def super_vendes_yesterday(self):
        query ="""
         select sum(amount_total) as value
         from sale_order so
         where date_order>=current_date- interval '1 day' and date_order<current_date;
        """
        self.env.cr.execute(query)
        res = self.env.cr.fetchall()

        query ="""
         select sum(amount_total) as value
         from sale_order so
         where date_order>=current_date- interval '8 day' and date_order<current_date- interval '7 days';
        """
        self.env.cr.execute(query)
        resp = self.env.cr.fetchall()

        result = {"value": res[0][0], "previous": resp[0][0]}
        return result

    def super_vendes_hora_total(self):
        query ="""
	    SELECT hour_order AS hour_order, sum(amount_total) AS amount_total
    	FROM
    	  (select 'sale' as type,
    	      so.name,
    	      so.state,
    	      cast(so.date_order as date) as date_order,
    	      date_part('hour', so.date_order) as hour_order ,
    	      so.amount_untaxed,
    	      so.amount_tax,
    	      so.amount_total,
    	      rp."name" as partner,
    	      pp1."name" as pricelist
    	   from sale_order so
    	   left join res_partner rp on rp.id = so.partner_id
    	   left join product_pricelist pp1 on so.pricelist_id = pp1.id
    	   union all select 'pos' as type,
    	                po.name,
    	                'sale' as state,
    	                cast(po.date_order as date) as date_order,
    	                date_part('hour', po.date_order) as hour_order ,
    	                po.amount_total - po.amount_tax as amount_untaxed,
    	                po.amount_tax,
    	                po.amount_total,
    	                rp."name" as partner,
    	                pp1."name" as pricelist
    	   from pos_order po
    	   left join res_partner rp on rp.id = po.partner_id
    	   left join product_pricelist pp1 on po.pricelist_id = pp1.id) AS virtual_table
    	GROUP BY hour_order
    	ORDER BY hour_order;
	    """
        self.env.cr.execute(query)
        res = self.env.cr.fetchall()
        hores = []
        vendes = []
        for x in res:
            hores.append('{:02d}'.format(int(x[0])))
            vendes.append(x[1])
        source = pandas.DataFrame(
                {
                   "hores": hores,
                    "vendes": vendes,
                }
             )
        chart = altair.Chart(source).mark_bar().encode(x="hores", y="vendes")
        text=chart.mark_text(align='center',  baseline='middle' ).encode( text="vendes")
        return {"altair": json.loads((chart+text).to_json())}


    def super_vendes_hora_30d(self):
        query ="""
	    SELECT hour_order AS hour_order, sum(amount_total) AS amount_total
    	FROM
    	  (select 'sale' as type,
    	      so.name,
    	      so.state,
    	      cast(so.date_order as date) as date_order,
    	      date_part('hour', so.date_order) as hour_order ,
    	      so.amount_untaxed,
    	      so.amount_tax,
    	      so.amount_total,
    	      rp."name" as partner,
    	      pp1."name" as pricelist
    	   from sale_order so
    	   left join res_partner rp on rp.id = so.partner_id
    	   left join product_pricelist pp1 on so.pricelist_id = pp1.id
    	   union all select 'pos' as type,
    	                po.name,
    	                'sale' as state,
    	                cast(po.date_order as date) as date_order,
    	                date_part('hour', po.date_order) as hour_order ,
    	                po.amount_total - po.amount_tax as amount_untaxed,
    	                po.amount_tax,
    	                po.amount_total,
    	                rp."name" as partner,
    	                pp1."name" as pricelist
    	   from pos_order po
    	   left join res_partner rp on rp.id = po.partner_id
    	   left join product_pricelist pp1 on po.pricelist_id = pp1.id) AS virtual_table
    	WHERE date_order >= current_date- interval '30 days'
    	  AND date_order < current_date
    	GROUP BY hour_order
    	ORDER BY hour_order;
	    """
        self.env.cr.execute(query)
        res = self.env.cr.fetchall()
        hores = []
        vendes = []
        for x in res:
            hores.append('{:02d}'.format(int(x[0])))
            vendes.append(x[1])
        source = pandas.DataFrame(
                {
                   "hores": hores,
                    "vendes": vendes,
                }
             )
        chart = altair.Chart(source).mark_bar().encode(x="hores", y="vendes")
        text=chart.mark_text(align='center',  baseline='middle' ).encode( text="vendes")
        return {"altair": json.loads((chart+text).to_json())}


    def super_vendes_hora_yesterday(self):
        query ="""
	    SELECT hour_order AS hour_order, sum(amount_total) AS amount_total
    	FROM
    	  (select 'sale' as type,
    	      so.name,
    	      so.state,
    	      cast(so.date_order as date) as date_order,
    	      date_part('hour', so.date_order) as hour_order ,
    	      so.amount_untaxed,
    	      so.amount_tax,
    	      so.amount_total,
    	      rp."name" as partner,
    	      pp1."name" as pricelist
    	   from sale_order so
    	   left join res_partner rp on rp.id = so.partner_id
    	   left join product_pricelist pp1 on so.pricelist_id = pp1.id
    	   union all select 'pos' as type,
    	                po.name,
    	                'sale' as state,
    	                cast(po.date_order as date) as date_order,
    	                date_part('hour', po.date_order) as hour_order ,
    	                po.amount_total - po.amount_tax as amount_untaxed,
    	                po.amount_tax,
    	                po.amount_total,
    	                rp."name" as partner,
    	                pp1."name" as pricelist
    	   from pos_order po
    	   left join res_partner rp on rp.id = po.partner_id
    	   left join product_pricelist pp1 on po.pricelist_id = pp1.id) AS virtual_table
    	WHERE date_order >= current_date- interval '1 days'
    	  AND date_order < current_date
    	GROUP BY hour_order
    	ORDER BY hour_order;
	    """
        self.env.cr.execute(query)
        res = self.env.cr.fetchall()
        hores = []
        vendes = []
        for x in res:
            hores.append('{:02d}'.format(int(x[0])))
            vendes.append(x[1])
        source = pandas.DataFrame(
                {
                   "hores": hores,
                    "vendes": vendes,
                }
             )
        chart = altair.Chart(source).mark_bar().encode(x="hores", y="vendes")
        text=chart.mark_text(align='center',  baseline='middle' ).encode( text="vendes")
        return {"altair": json.loads((chart+text).to_json())}


    def super_vendes_daily_30d(self):
        query ="""
	    SELECT  date_part('year', date_order)*10000+date_part('month', date_order)*100+date_part('day', date_order) AS date_order, sum(amount_total) AS amount_total
    	FROM
    	  (select 'sale' as type,
    	      so.name,
    	      so.state,
    	      cast(so.date_order as date) as date_order,
    	      so.amount_untaxed,
    	      so.amount_tax,
    	      so.amount_total,
    	      rp."name" as partner,
    	      pp1."name" as pricelist
    	   from sale_order so
    	   left join res_partner rp on rp.id = so.partner_id
    	   left join product_pricelist pp1 on so.pricelist_id = pp1.id
    	   union all select 'pos' as type,
    	                po.name,
    	                'sale' as state,
    	                cast(po.date_order as date) as date_order,
    	                po.amount_total - po.amount_tax as amount_untaxed,
    	                po.amount_tax,
    	                po.amount_total,
    	                rp."name" as partner,
    	                pp1."name" as pricelist
    	   from pos_order po
    	   left join res_partner rp on rp.id = po.partner_id
    	   left join product_pricelist pp1 on po.pricelist_id = pp1.id) AS virtual_table
    	WHERE date_order >= current_date- interval '30 days'
    	  AND date_order < current_date
    	GROUP BY date_order
    	ORDER BY date_order;
	    """

        self.env.cr.execute(query)
        res = self.env.cr.fetchall()
        dies = []
        vendes = []
        for x in res:
            dies.append('{:02d}'.format(int(x[0])))
            vendes.append(x[1])
        source = pandas.DataFrame(
                {
                   "dies": dies,
                    "vendes": vendes,
                }
             )
        chart = altair.Chart(source).mark_bar().encode(x="dies", y="vendes")
        text=chart.mark_text(align='center',  baseline='middle' ).encode( text="vendes")
        return {"altair": json.loads((chart+text).to_json())}


    def x_test_num(self):
        query ="""
         select sum(amount_total) as value
         from sale_order so
         where date_order between date_trunc('week', current_date - interval '7 day'  )
            and date_trunc('week', current_date - interval '7 day'  ) + interval '6 day';
        """
        self.env.cr.execute(query)
        res = self.env.cr.fetchall()
        result = {"value": res[0][0]}
        return result

    def x_socies_actuals(self):
        query ="""
        SELECT
	       max(socies_acumulades) AS value
    	FROM
    	  (select coalesce (al.date_invoice,ba.date_invoice) as date,
    	     coalesce (altes,0) as altes,
    		 coalesce (baixes,0) as baixes,
    		 coalesce (altes, 0) - coalesce (baixes,0) as altes_netes,
    		 sum(coalesce (altes, 0) - coalesce (baixes, 0)) OVER ( ORDER BY coalesce (al.date_invoice,ba.date_invoice)) AS socies_acumulades
    	   from
    	     (select a.date_invoice,count(*) as baixes
    	      from
        		(select ai.date_invoice
        		 from account_invoice ai
        		     join res_partner rp on ai.partner_id =rp.id
        		     join account_journal aj on ai.journal_id =aj.id
        		 where ai."type" ='out_refund'
        		     and aj."name" = 'Subscription Journal' ) a
        	     group by a.date_invoice
             ) ba
    	     full join
    	     (select date_invoice, count(*) as altes
    	     from
        		(select ai.date_invoice
        		 from account_invoice ai
        		     join res_partner rp on ai.partner_id =rp.id
        		     join account_journal aj on ai.journal_id =aj.id
        		 where ai."type" = 'out_invoice'
        		     and aj."name" = 'Subscription Journal'
        		     and ai.state ='paid' ) b
        	      group by date_invoice
             ) al on al.date_invoice=ba.date_invoice
         ) AS virtual_table;
       """
        self.env.cr.execute(query)
        res = self.env.cr.fetchall()
        result = {"value": res[0][0]}
        return result

    def x_socies_altes(self):
        query ="""
        SELECT  sum(altes) AS "altes"
	FROM
	  (select coalesce (al.date_invoice,ba.date_invoice) as date,
         coalesce (altes,0) as altes,
         coalesce (baixes,0) as baixes,
         coalesce (altes, 0) - coalesce (baixes,0) as altes_netes,
         sum(coalesce (altes, 0) - coalesce (baixes, 0)) OVER ( ORDER BY coalesce (al.date_invoice,ba.date_invoice)) AS socies_acumulades
       from
	     (select a.date_invoice, count(*) as baixes
	      from
		      (select ai.date_invoice
		      from account_invoice ai
		          join res_partner rp on ai.partner_id =rp.id
		          join account_journal aj on ai.journal_id =aj.id
		      where ai."type" ='out_refund'
		          and aj."name" = 'Subscription Journal' ) a
	          group by a.date_invoice) ba
	     full join
	     (select date_invoice, count(*) as altes
	      from
		      (select ai.date_invoice
		      from account_invoice ai
		          join res_partner rp on ai.partner_id =rp.id
		          join account_journal aj on ai.journal_id =aj.id
		      where ai."type" = 'out_invoice'
		          and aj."name" = 'Subscription Journal'
		          and ai.state ='paid' ) b
	          group by date_invoice) al on al.date_invoice=ba.date_invoice) AS virtual_table
	    WHERE date >= TO_TIMESTAMP('2021-10-28 00:00:00.000000', 'YYYY-MM-DD HH24:MI:SS.US')
	          AND date < TO_TIMESTAMP('2022-01-28 00:00:00.000000', 'YYYY-MM-DD HH24:MI:SS.US');
       """
        self.env.cr.execute(query)
        res = self.env.cr.fetchall()
        result = {"value": res[0][0]}
        return result
